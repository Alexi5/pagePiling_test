var flag = true;

$(document).ready(function() {
	$('#pagepiling').pagepiling({
            scrollingSpeed: 700,

            //events
            afterLoad: null,
            onLeave: function(){
            if (flag){
                  $("#section1Column").css({"filter":"blur(5px)"});

                  $("#section1Column").animate([
                        {transform: "scale(0.5)"},
                        {transform: "scale(1.5)"}],2000);
                  flag = false;  

            }else{
                  $("#section1Column").css({"filter":"blur(0px)"});
                  flag=true;
               }
            },
            afterRender: null
	});
	defaultSize();
});

// Setting the size of each window
function defaultSize(){
	var windowHeight = $(window).height();
	$(".section").css({ "min-height" : windowHeight+"px"})
}

